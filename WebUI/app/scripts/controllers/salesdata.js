'use strict';

angular.module('SPAAuthenticationExampleApp')
  .controller('SalesDataCtrl', ['$scope', '$resource', function ($scope, $resource) {
    $scope.salesSummaryByRegion = $resource('http://192.168.1.44:42042/regions/summary').query();
  }]);
