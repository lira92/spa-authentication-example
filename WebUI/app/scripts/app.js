'use strict';

angular.module('SPAAuthenticationExampleApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.router'
])
  .config(function ($stateProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('main',
      {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('login',
      {
        templateUrl: 'views/login-form.html',
        controller: 'LoginCtrl'
      })
      .state('logout',
      {
        controller: 'LogoutCtrl'
      })
      .state('sales', {
        templateUrl: 'views/sales-data.html',
        controller: 'SalesDataCtrl',
        resolve: {
          user: 'User',
          authenticationRequired: function(user) {
            user.isAuthenticated();
          }
        }
      });
  })
  .run(function($rootScope, $state, User) {
    try  {
      User.isAuthenticated();
    } catch(e) {
      // do nothing with this error
    }
    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
      if (error.name === 'AuthenticationRequired') {
        User.setNextState(toState.name, 'You must login to access this page');
        $state.go('login', {}, {reload: true});
      }
    });
  });
